namespace App\Repositories;

use App\Models\{{$entity}};
{{--
    Laravel inserts two spaces between @property and type, so we are forced
    to use hack here to preserve one space
--}}
@php
echo <<<PHPDOC
/**
 * @property {$entity} \$model
 */

PHPDOC;
@endphp
class {{$entity}}Repository extends Repository
{
    public function __construct()
    {
        $this->setModel({{$entity}}::class);
    }
}
